<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Student;
use Faker\Generator as Faker;

$factory->define(Student::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'class' => $faker->word,
        'group' => $faker->word,
        'email' => $faker->safeEmail,
    ];
});
