<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\TeacherRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use DB;
use Hash;
/**
 * Class TeacherCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class TeacherCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\InlineCreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\FetchOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Teacher::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/teacher');
        CRUD::setEntityNameStrings('teacher', 'teachers');
    }
    public function fetchUser()
    {
        dd($this->fetch(\App\User::class));
        return $this->fetch(\App\User::class);
    }
    public function fetchStudent()
    {
        dd($this->fetch(\App\Models\Student::class));
        return $this->fetch(\App\Models\Student::class);
    }
    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        CRUD::setFromDb(); // columns
        $this->crud->enableExportButtons();

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(TeacherRequest::class);

      //  CRUD::field('First name')->type('text');
       // CRUD::field('Last name')->type('text');
        CRUD::addField(
            [  // Select2
                'label'     => "Classe",
                'type'      => 'select2',
                'name'      => 'classe_id', // the db column for the foreign key

                // optional
             //  'entity'    => 'group', // the method that defines the relationship in your Model
                'model'     => "App\Models\Classe", // foreign key model
                'attribute' => 'name', // foreign key attribute that is shown to user
                'default'   => 2, // set the default value of the select2

                 // also optional
                'options'   => (function ($query) {
                     return $query->orderBy('name', 'ASC')->get();
                 }), // force the related options to be a custom query, instead of all(); you can use this to filter the results show in the select

                 ]
                );
        CRUD::addField(
            [  // Select2
                'label'     => "Group",
                'type'      => 'select2',
                'name'      => 'group_id', // the db column for the foreign key

                // optional
             //  'entity'    => 'group', // the method that defines the relationship in your Model
                'model'     => "App\Models\Group", // foreign key model
                'attribute' => 'name', // foreign key attribute that is shown to user
                'default'   => 2, // set the default value of the select2

                 // also optional
                'options'   => (function ($query) {
                     return $query->orderBy('name', 'ASC')->get();
                 }), // force the related options to be a custom query, instead of all(); you can use this to filter the results show in the select
             ]
                );

              /*   CRUD::addField([    // Relationship
                    'label'     => 'user',
                    'type'      => 'relationship',
                    'name'      => 'user',
                    'entity'    => 'user',
                    'attribute' => 'name',
                    //'model'             => "App\Models\Tag",
                    // 'tab'       => 'Relationships',

                    // 'inline_create' => true, // TODO: make it work like this too
                    'inline_create'     => [
                        'entity'      => 'user',
                        'modal_class' => 'modal-dialog modal-xl',
                    ],
                    'data_source'       => backpack_url('teacher/fetch/student'),
                    'minimum_input_length'    => 0,
                    // 'wrapperAttributes' => ['class' => 'form-group col-md-12'],
                 ]); */


                 CRUD::field('password')->type('password');
                 CRUD::field('test')->type('text');
        CRUD::setFromDb(); // fields
        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

    public function create()
    {
        $this->crud->hasAccessOrFail('create');

        // prepare the fields you need to show
        $this->data['crud'] = $this->crud;
        \Debugbar::info("fffff");
        \Debugbar::info( $this->data['crud']);
        $this->data['saveAction'] = $this->crud->getSaveAction();
        $this->data['title'] = $this->crud->getTitle() ?? trans('backpack::crud.add').' '.$this->crud->entity_name;

        // load the view from /resources/views/vendor/backpack/crud/ if it exists, otherwise load the one in the package
        return view($this->crud->getCreateView(), $this->data);
    }
    public function store()
    {
        $this->crud->hasAccessOrFail('create');

        // execute the FormRequest authorization and validation, if one is required
        $request = $this->crud->validateRequest();

        // insert item in the db
        $item = $this->crud->create($this->crud->getStrippedSaveRequest());
        $this->data['entry'] = $this->crud->entry = $item;
        $data=$this->crud->getStrippedSaveRequest();

        // show a success message

        // save the redirect choice for next time
        DB::table('users')->insert([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);

        \Debugbar::info($data['password']);
        \Debugbar::info($data['name']);
        $this->crud->setSaveAction();


        return $this->crud->performSaveAction($item->getKey());
    }

}
