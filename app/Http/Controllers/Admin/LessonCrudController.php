<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\LessonRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class LessonCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class LessonCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Lesson::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/lesson');
        CRUD::setEntityNameStrings('lesson', 'lessons');
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
    //  CRUD::setFromDb(); // columns
      CRUD::column('name')->type('text');
      CRUD::column('description')->type('text');
      CRUD::column('file')->type('text');
      CRUD::column('start_time')->type('date');
      CRUD::column('end_time')->type('date');
         CRUD::column('file')->wrapper([
                        // 'element' => 'a', // the element will default to "a" so you can skip it here
                        'href' => function ($crud, $column, $entry, $related_key) {
                            return url('/'.$entry['file']);
                        },
                        'target' => '_blank',
                        // 'class' => 'some-class',
                    ]);
        $this->crud->enableExportButtons();
        CRUD::addColumn([
            'label' => 'Classe',
            'type' => 'relationship',
            'name' => 'classe', //name method in the model
            'entity' => 'classe', //name method in the model
            'attribute' => 'name' ,// attribute in the database
        ]);    CRUD::addColumn([
            'label' => 'Group',
            'type' => 'relationship',
            'name' => 'group', //name method in the model
            'entity' => 'group', //name method in the model
            'attribute' => 'name' ,// attribute in the database
        ]);
        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(LessonRequest::class);
        CRUD::field('name');
        CRUD::addField(
            [  // Select2
                'label'     => "Classe",
                'type'      => 'select2',
                'name'      => 'classe_id', // the db column for the foreign key
                // optional
             //  'entity'    => 'group', // the method that defines the relationship in your Model
                'model'     => "App\Models\Classe", // foreign key model
                'attribute' => 'name', // foreign key attribute that is shown to user
                'default'   => 2, // set the default value of the select2
                 // also optional
                'options'   => (function ($query) {
                     return $query->orderBy('name', 'ASC')->get();
                 }), // force the related options to be a custom query, instead of all(); you can use this to filter the results show in the select
             ]
                );
        CRUD::addField(
            [  // Select2
                'label'     => "Group",
                'type'      => 'select2',
                'name'      => 'group_id', // the db column for the foreign key

                // optional
             //  'entity'    => 'group', // the method that defines the relationship in your Model
                'model'     => "App\Models\Group", // foreign key model
                'attribute' => 'name', // foreign key attribute that is shown to user
                'default'   => 2, // set the default value of the select2

                 // also optional
                'options'   => (function ($query) {
                     return $query->orderBy('name', 'ASC')->get();
                 }), // force the related options to be a custom query, instead of all(); you can use this to filter the results show in the select
             ]
                );

        CRUD::setFromDb(); // fields
        CRUD::field('file')->type('browse')->label('Document');
        CRUD::field('description')->type('textarea');
        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
